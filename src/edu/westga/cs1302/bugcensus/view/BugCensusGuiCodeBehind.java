package edu.westga.cs1302.bugcensus.view;

import edu.westga.cs1302.bugcensus.view.converter.NonnegativeWholeNumberStringConverter;
import edu.westga.cs1302.bugcensus.view.converter.NonnegativeDecimalNumberStringConverter;
import java.io.File;
import java.util.List;

import edu.westga.cs1302.bugcensus.model.Insect;
import edu.westga.cs1302.bugcensus.model.Bee;
import edu.westga.cs1302.bugcensus.model.Myriapoda;
import edu.westga.cs1302.bugcensus.model.BeeCaste;
import edu.westga.cs1302.bugcensus.model.Bug;
import edu.westga.cs1302.bugcensus.model.BugType;
import edu.westga.cs1302.bugcensus.viewmodel.BugCensusViewModel;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 * The Class BugCensusCodeBehind.
 * 
 * @author CS1302
 * @version Fall 2018
 */
public class BugCensusGuiCodeBehind {

	private BugCensusViewModel viewmodel;
	
	@FXML
	private AnchorPane mainPane;

	@FXML
	private Label wordRequiredFormatLabel;

	@FXML
	private Label frequencyRequiredFormatLabel;

	@FXML
	private ListView<Bug> bugsListView;

	@FXML
	private MenuItem removeContextMenuItem;

	@FXML
	private Canvas bugCanvas;
	
	@FXML
    private Pane bugsPane;

	@FXML
	private TextArea reportTextArea;
	
	@FXML
    private ComboBox<BugType> bugTypeComboBox;

	@FXML
    private ComboBox<BeeCaste> beeCasteComboBox;
	
    @FXML
    private TextField lengthTextField;

    @FXML
    private TextField numberLegsTextField;

    @FXML
    private TextField numberSegmentsTextField;

    @FXML
    private CheckBox wingedCheckBox;
    
    @FXML
    private ColorPicker bugColorPicker;

    @FXML
    private Button addBugButton;


	/**
	 * Instantiates BugCensusGuiCodeBehind.
	 *
	 * @precondition none
	 * @postcondition none
	 */
	public BugCensusGuiCodeBehind() {
	}
	
	@FXML
	private void initialize() {
		this.viewmodel = new BugCensusViewModel(this.bugsPane);
		this.bugsListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		this.setupBindings();
		this.setupDisableBindings();
		this.setupSelectionHandlers();
		this.setupListenersForValidation();
		this.resetBugFormValues();
		this.styling();
	}
	
	/**
	 * Sets up the bindings.
	 */
	private void setupBindings() {
		this.bugsListView.itemsProperty().bind(this.viewmodel.bugsProperty());
		this.reportTextArea.textProperty().bindBidirectional(this.viewmodel.reportProperty());
		this.bugTypeComboBox.itemsProperty().bind(this.viewmodel.bugTypeProperty());
		this.viewmodel.selectedBugTypeProperty().bind(this.bugTypeComboBox.getSelectionModel().selectedItemProperty());
		this.beeCasteComboBox.itemsProperty().bind(this.viewmodel.beeCasteProperty());
		this.viewmodel.selectedBeeCasteProperty().bind(this.beeCasteComboBox.getSelectionModel().selectedItemProperty());
		this.lengthTextField.textProperty().bindBidirectional(this.viewmodel.lengthProperty(),
				new NonnegativeDecimalNumberStringConverter(1));
		this.numberLegsTextField.textProperty().bindBidirectional(this.viewmodel.numberLegsProperty(),
				new NonnegativeWholeNumberStringConverter());
		this.numberSegmentsTextField.textProperty().bindBidirectional(this.viewmodel.numberSegmentsProperty(),
				new NonnegativeWholeNumberStringConverter());
	    this.wingedCheckBox.selectedProperty().bindBidirectional(this.viewmodel.wingedProperty());
	    this.bugColorPicker.valueProperty().bindBidirectional(this.viewmodel.bugColorProperty());
	}
	
	private void setupDisableBindings() {
		BooleanBinding myriapodaNotSelected = Bindings.notEqual(this.bugTypeComboBox.getSelectionModel().selectedItemProperty(), BugType.MYRIAPODA);
		this.numberSegmentsTextField.disableProperty().bind(myriapodaNotSelected);
		this.numberLegsTextField.disableProperty().bind(myriapodaNotSelected);
		BooleanBinding beeNotSelected = Bindings.notEqual(this.bugTypeComboBox.getSelectionModel().selectedItemProperty(), BugType.BEE);
		this.beeCasteComboBox.disableProperty().bind(beeNotSelected);
		BooleanBinding beeSelected = Bindings.equal(this.bugTypeComboBox.getSelectionModel().selectedItemProperty(), BugType.BEE);
		this.bugColorPicker.disableProperty().bind(beeSelected);
		BooleanBinding insectNotSelected = Bindings.notEqual(this.bugTypeComboBox.getSelectionModel().selectedItemProperty(), BugType.INSECT);
		this.wingedCheckBox.disableProperty().bind(insectNotSelected);
	}
	
	private void setupListenersForValidation() {
		this.lengthTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue != null) {
				if (newValue.startsWith(".")) {
					newValue = "0" + newValue;
					BugCensusGuiCodeBehind.this.lengthTextField.setText(newValue);
				}
				if (!newValue.matches("[\\d]*([.]\\d{0,3})?")) {
					BugCensusGuiCodeBehind.this.lengthTextField.setText(oldValue);
				}
			}
		});
		
		this.numberLegsTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue != null) {
				if (!newValue.matches("\\d{0,1}|[1-9]\\d*")) {
					BugCensusGuiCodeBehind.this.numberLegsTextField.setText(oldValue);
				}
			}
		});
		
		this.numberSegmentsTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue != null) {
				if (!newValue.matches("\\d{0,1}|[1-9]\\d*")) {
					BugCensusGuiCodeBehind.this.numberSegmentsTextField.setText(oldValue);
				}
			}
		});
	}
	
	private void setupSelectionHandlers() {
		this.bugsListView.getSelectionModel().selectedItemProperty().addListener((observable, oldBug, newBug) -> {
			List<Bug> selectedBugs = this.bugsListView.getSelectionModel().getSelectedItems();
			if (selectedBugs != null && !selectedBugs.isEmpty()) {
				if (selectedBugs.size() == 1) {
					this.viewmodel.handleBugSelection(newBug);
					this.setBugFormValues(newBug);
				} else {
					this.viewmodel.handleBugSelection(selectedBugs);
					this.resetBugFormValues();
				}
			}
		});
		
		this.bugTypeComboBox.getSelectionModel().selectedItemProperty().addListener((observable, oldBugType, newBugType) -> {
			if (newBugType != null) {
				if (newBugType == BugType.INSECT || newBugType == BugType.BEE) {
					this.numberLegsTextField.setText("6");
					this.numberSegmentsTextField.setText("");
				} 
				if (newBugType == BugType.BEE) {
					this.wingedCheckBox.setSelected(true);
				}
				if (newBugType == BugType.MYRIAPODA) {
					this.wingedCheckBox.setSelected(false);
				}
			}
		});
	}
	
	private void resetBugFormValues() {
		this.bugTypeComboBox.getSelectionModel().select(BugType.INSECT);
		this.beeCasteComboBox.getSelectionModel().select(BeeCaste.WORKER);
		this.bugColorPicker.setValue(Color.BLACK);
		this.lengthTextField.setText("0.0");
		this.numberLegsTextField.setText("0");
		this.numberSegmentsTextField.setText("0");
		this.wingedCheckBox.setSelected(false);
	}
	
	private void setBugFormValues(Bug bug) {
		BugType bugType = BugType.getType(bug);
		this.bugTypeComboBox.getSelectionModel().select(bugType);
		this.bugColorPicker.setValue(bug.getColor());
		this.lengthTextField.setText(Double.valueOf(bug.getLength()).toString());
		this.numberLegsTextField.setText(Integer.valueOf(bug.getNumberLegs()).toString());
		if (bugType == BugType.BEE) {
			this.beeCasteComboBox.getSelectionModel().select(((Bee) bug).getCaste());
		} else {
			this.beeCasteComboBox.getSelectionModel().select(BeeCaste.WORKER);
		}
		if (bugType == BugType.MYRIAPODA) {
			Integer numberLegs = ((Myriapoda) bug).getNumberSegments();
			this.numberSegmentsTextField.setText(numberLegs.toString());
		} else {
			this.numberSegmentsTextField.setText("0");
		}
		if (bugType == BugType.INSECT) {
			this.wingedCheckBox.setSelected(((Insect) bug).isWinged());
		} else {
			this.wingedCheckBox.setSelected(false);
		}
	}
	
	private void styling() {
		this.bugsPane.setBorder(new Border(new BorderStroke(Color.BLACK, 
	            BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
	}

	@FXML
	void handleFileOpen(ActionEvent event) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Text File");
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("Census Files", "*.bcs"),
				new ExtensionFilter("All Files", "*.*"));

		Stage stage = (Stage) this.mainPane.getScene().getWindow();
		File selectedFile = fileChooser.showOpenDialog(stage);
		if (selectedFile != null) {
			this.viewmodel.loadCensusData(selectedFile);
		}
	}

	@FXML
	void handleAddBug(ActionEvent event) {
		try {
			this.viewmodel.addBug();
		} catch (IllegalArgumentException e) {
			this.showAlert("Add Bug", e.getMessage());
		}
	}

	@FXML
	void handleGenerateSummaryReport(ActionEvent event) {
		this.viewmodel.generateSummaryReport();
	}

	@FXML
	void handleGenerateDetailedReport(ActionEvent event) {
		this.viewmodel.generateDetailedReport();
	}
	
	private void showAlert(String title, String message) {
		Alert alert = new Alert(AlertType.INFORMATION);
		Window owner = this.mainPane.getScene().getWindow();
		alert.initOwner(owner);
		alert.setTitle(title);
		alert.setContentText(message);
		alert.showAndWait();
	}
}
