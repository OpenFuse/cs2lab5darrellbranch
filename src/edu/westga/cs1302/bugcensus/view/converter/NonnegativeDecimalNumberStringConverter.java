package edu.westga.cs1302.bugcensus.view.converter;

import java.text.DecimalFormat;
import edu.westga.cs1302.bugcensus.resources.UI;
import javafx.util.converter.NumberStringConverter;

/**
 * The Class ValidWholeNumberStringConverter.
 * 
 * @author CS1302
 */
public class NonnegativeDecimalNumberStringConverter extends NumberStringConverter {

	private static final String ZERO_STARTING_DECIMAL = "0.";
	private int numberDecimals = 0;

	/**
	 * Instantiates a new positive decimal number string converter with specified
	 * number of decimal places.
	 * 
	 * @precondition numberDecimals >= 0
	 *
	 * @param numberDecimals the number decimal places
	 */
	public NonnegativeDecimalNumberStringConverter(int numberDecimals) {
		if (numberDecimals < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.INVALID_NUMBERDECIMALS);
		}

		this.numberDecimals = numberDecimals;
	}

	/**
	 * Removes all non-digits from the string and then converts it to a number.
	 * 
	 * @param input The string to convert
	 */
	@Override
	public Number fromString(String input) {
		if (input.equals(".")) {
			input = ZERO_STARTING_DECIMAL;
		}
		input = input.replaceAll("[^(\\d|.)]", "");
		return super.fromString(input);
	}

	/**
	 * Converts number to string with two decimal digits.
	 * 
	 * @param value The number to convert
	 */
	@Override
	public String toString(Number value) {
		String zeros = new String(new char[this.numberDecimals]).replace('\0', '0');
		DecimalFormat df = new DecimalFormat(ZERO_STARTING_DECIMAL + zeros);
		String output = df.format(value);
		return output;
	}

}
