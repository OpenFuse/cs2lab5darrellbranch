/*
 * 
 */
package edu.westga.cs1302.bugcensus.model;

import java.util.ArrayList;
import java.util.Calendar;

import edu.westga.cs1302.bugcensus.resources.UI;

/**
 * The Class BugData.
 * 
 * @author CS1302
 * @version Fall 2018
 */
public class BugCensus {
	
	private int year;
	private ArrayList<Bug> bugs;
	private int numberInsects;
	private int numberMyriapodas;
	
	/**
	 * Instantiates a new bug census.
	 *
	 * @precondition none
	 * @postcondition getBugs.size() == 0 && getNumberInsects() == 0 && getNUmberMyriapods() == 0
	 */
	public BugCensus() {
		Calendar now = Calendar.getInstance();
		this.year = now.get(Calendar.YEAR);
		this.bugs = new ArrayList<Bug>();
		this.numberInsects = 0;
		this.numberMyriapodas = 0;
	}
	
	/**
	 * Instantiates a new bug census.
	 *
	 * @precondition year >= 0
	 * @postcondition getYear() == year && getBugs.size() == 0 && getNumberInsects() == 0 && getNUmberMyriapods() == 0
	 * 
	 * @param year the year
	 */
	public BugCensus(int year) {
		if (year < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NEGATIVE_YEAR);
		}
		this.year = year;
		this.bugs = new ArrayList<Bug>();
		this.numberInsects = 0;
		this.numberMyriapodas = 0;
	}

	/**
	 * Gets the year.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the year
	 */
	public int getYear() {
		return this.year;
	}

	/**
	 * Sets the year.
	 *
	 * @precondition year >= 0
	 * @postcondition getYear() == year
	 * 
	 * @param year the new year
	 */
	public void setYear(int year) {
		if (year < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NEGATIVE_YEAR);
		}
		this.year = year;
	}

	/**
	 * Gets the bugs.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the bugs
	 */
	public ArrayList<Bug> getBugs() {
		return this.bugs;
	}
	
	
	/**
	 * returns the size.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the size
	 */
	public int size() {
		return this.bugs.size();
	}
	
	/**
	 * Adds the bug.
	 *
	 * @precondition bug != null
	 * @postcondition size() == size()@prev + 1
	 * 
	 * @param bug the bug
	 * @return true, if successful
	 */
	public boolean add(Bug bug) {
		if (bug == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_BUG);
		}
		if (bug instanceof Insect) {
			this.numberInsects++;
		} else if (bug instanceof Myriapoda) {
			this.numberMyriapodas++;
		}
		return this.bugs.add(bug);
	}
	
	/**
	 * Adds the insect.
	 *
	 * @precondition insect != null
	 * @postcondition size() == size()@prev + 1
	 * 
	 * @param insect the insect
	 * @return true, if successful
	 */
	public boolean add(Insect insect) {
		if (insect == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_INSECT);
		}
		if (this.bugs.add(insect)) {
			this.numberInsects++;
			return true;
		}
		return false;
	}
	
	/**
	 * Adds the myriapoda.
	 *
	 * @precondition myriapoda != null
	 * @postcondition size() == size()@prev + 1
	 * 
	 * @param myriapoda the myriapoda
	 * @return true, if successful
	 */
	public boolean add(Myriapoda myriapoda) {
		if (myriapoda == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_INSECT);
		}
		if (this.bugs.add(myriapoda)) {
			this.numberMyriapodas++;
			return true;
		}
		return false;
	}

	/**
	 * Gets the number insects.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the number insects
	 */
	public int getNumberInsects() {
		return this.numberInsects;
	}

	/**
	 * Gets the number myriapodas.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the number myriapodas
	 */
	public int getNumberMyriapodas() {
		return this.numberMyriapodas;
	}
	
	/**
	 * Gets the summary report.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the summary report
	 */
	public String getSummaryReport() {
		String report = "Bug Census of " + year;
		report += System.lineSeparator();
		report += "Total number bugs: " + this.size();
		report += System.lineSeparator();
		report += "Number insects: " + this.getNumberInsects();
		report += System.lineSeparator();
		report += "Total myriapodias: " + this.getNumberMyriapodas();
		return report;
	}
}
