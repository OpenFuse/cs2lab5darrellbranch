package edu.westga.cs1302.bugcensus.model;

import edu.westga.cs1302.bugcensus.resources.UI;
import javafx.scene.paint.Color;

/**
 * The Class Bug.
 * 
 * @author CS1302
 * @version Fall 2018
 */
public abstract class Bug implements Drawable {
	
	private double length;
	private int numberLegs;
	private Color color;
	
	/**
	 * Instantiates a new bug.
	 *
	 * @precondition length > 0 and numberLegs >= 0 and color != null
	 * @postcondition getLength() == length && getNumberLegs() == numberLegs &&
	 *                getColor() == color
	 * 
	 * @param length the length
	 * @param numberLegs the number of legs
	 * @param color the color
	 */
	protected Bug(double length, int numberLegs, Color color) {
		if (length <= 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NONPOSITIVE_LENGTH);
		}
		if (numberLegs < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NEGATIVE_NUMBER_LEGS);
		}
		if (color == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_COLOR);
		}
		this.length = length;
		this.numberLegs = numberLegs;
		this.color = color;
	}

	/**
	 * Gets the length.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the length
	 */
	public double getLength() {
		return this.length;
	}

	/**
	 * Sets the length.
	 *
	 * @precondition length > 0
	 * @postcondition none
	 * 
	 * @param length the new length
	 */
	public void setLength(double length) {
		if (length <= 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NONPOSITIVE_LENGTH);
		}
		this.length = length;
	}

	/**
	 * Gets the number of legs.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the number of legs
	 */
	public int getNumberLegs() {
		return this.numberLegs;
	}

	/**
	 * Sets the number of legs.
	 *
	 * @precondition numberLegs >= 0
	 * @postcondition none
	 * 
	 * @param numberLegs the new number of legs
	 */
	public void setNumberLegs(int numberLegs) {
		if (numberLegs < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NEGATIVE_NUMBER_LEGS);
		}
		this.numberLegs = numberLegs;
	}

	/**
	 * Gets the color.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the color
	 */
	public Color getColor() {
		return this.color;
	}

	/**
	 * Sets the color.
	 *
	 * @precondition color != null
	 * @postcondition getColor() == color
	 * 
	 * @param color the new color
	 */
	public void setColor(Color color) {
		if (color == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_COLOR);
		}
		this.color = color;
	}
	
	/**
	 * Gets the description.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the description
	 */
	public String getDescription() {
		String description = "Length: " + this.length;
		description += System.lineSeparator();
		description += "Number legs: " + this.numberLegs;
		description += System.lineSeparator();
		description += "Color: " + this.color;
		
		return description;
	}
	
	/**
	 * Gets the drawing width.
	 *
	 * @return the drawing width of the drawing
	 */
	public double getDrawingWidth() {
		return this.length;
	}
	
	/**
	 * Gets the drawing height.
	 *
	 * @return the drawing height of the drawing
	 */
	public double getDrawingHeight() {
		return this.length;
	}
	
	@Override
	public String toString() {
		return "Bug length=" + this.length + " #legs=" + this.numberLegs + " color=" + this.color;
	}
	
}
