package edu.westga.cs1302.bugcensus.model;

import javafx.scene.canvas.Canvas;

/**
 * The Interface Drawable.
 * @author Darrell Branch
 */
public interface Drawable {
	
	/**
	 * Gets the drawing.
	 *
	 * @return the drawing
	 */
	Canvas getDrawing();
	
	/**
	 * Gets the drawing.
	 *
	 * @param xCoord the xcoordinate of the upper left corner
	 * @param yCoord the ycoordinate of the upper left corner
	 * @return the drawing
	 */
	Canvas getDrawing(double xCoord, double yCoord);
	
	/**
	 * Gets the drawing width.
	 *
	 * @return the drawing width of the drawing
	 */
	double getDrawingWidth();
	
	/**
	 * Gets the drawing height.
	 *
	 * @return the drawing height of the drawing
	 */
	double getDrawingHeight();
	

}
