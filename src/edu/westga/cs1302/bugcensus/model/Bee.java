package edu.westga.cs1302.bugcensus.model;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * The Class Bee.
 * 
 * @author CS1302
 * @version Fall 2018
 */
public class Bee extends Insect {
	private BeeCaste caste;
	
	/**
	 * Instantiates a new bee.
	 * 
	 * @precondition length > 0
	 * @postcondition getLength() == length && getNumberLegs() == 6 && getColor() ==
	 *                Color.BLACK && isWinged() == true && getCaste() == caste
	 * 
	 * @param length the length
	 * @param caste the caste
	 */
	public Bee(double length, BeeCaste caste) {
		super(length, true, Color.BLACK);
		this.caste = caste;
	}
	
	/**
	 * Gets the type.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the type
	 */
	public BeeCaste getCaste() {
		return this.caste;
	}

	/**
	 * Sets the type.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param caste the new case
	 */
	public void setCaste(BeeCaste caste) {
		this.caste = caste;
	}
	
	/**
	 * Draw stripes.
	 *
	 * @param gc the gc
	 */
	public void drawStripes(GraphicsContext gc) {
		gc.setFill(Color.YELLOW);
		gc.fillRect(5, 35, 8, 28);
		gc.fillRect(30, 35, 8, 28);
		gc.fillRect(55, 35, 8, 28);
		gc.fillRect(80, 35, 8, 28);
	}
	
	/**
	 * Gets the drawing.
	 *
	 * @return the drawing
	 */
	public Canvas getDrawing() {
		Canvas canvas = super.getDrawing();
		GraphicsContext gc = canvas.getGraphicsContext2D();
		this.drawStripes(gc);
		return canvas;
	}
	
	/**
	 * Gets the drawing.
	 *
	 * @param xCoord the xcoordinate of the upper left corner
	 * @param yCoord the ycoordinate of the upper left corner
	 * @return the drawing
	 */
	public Canvas getDrawing(double xCoord, double yCoord) {
		Canvas canvas = super.getDrawing();
		GraphicsContext gc = canvas.getGraphicsContext2D();
		this.drawStripes(gc);
		return canvas;
	}
	
	@Override
	public String getDescription() {
		String description = super.getDescription();
		description += System.lineSeparator();
		description += "Caste: " + this.caste;
		
		return description;
	}

	@Override
	public String toString() {
		return "Bee length=" + this.getLength() + " caste=" + this.caste;
	}
}
