package edu.westga.cs1302.bugcensus.model;

import edu.westga.cs1302.bugcensus.resources.UI;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * The Class Myriapoda.
 * 
 * @author CS1302
 * @version Fall 2018
 */
public class Myriapoda extends Bug {
//	private static final double DEFAULT_HEIGHT = 50; 
//	private static final double DEFAULT_SEGMENT_WIDTH = 20; 
	private int numberSegments;
	
	/**
	 * Instantiates a new myriapoda.
	 *
	 * @precondition length > 0 && numberLegs >= 0 && numberSegments > 0 &&
	 *               color != null
	 * @postcondition getLength() == length && getNumberLegs() == numberLegs &&
	 *                getColor() == color && getNumberSegments() == numberSegments
	 * 
	 * @param length the length
	 * @param numberLegs the number legs
	 * @param numberSegments the number segments
	 * @param color the color
	 */
	public Myriapoda(double length, int numberLegs, int numberSegments, Color color) {
		super(length, numberLegs, color);
		if (numberSegments <= 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NEGATIVE_NUMBER_SEGMENTS);
		}
		this.numberSegments = numberSegments;
	}

	
	/**
	 * Gets the number of segments.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the number of segments
	 */
	public int getNumberSegments() {
		return this.numberSegments;
	}
	
	/**
	 * Draw.
	 *
	 * @param gc the gc
	 */
	public void draw(GraphicsContext gc) {
		gc.setFill(this.getColor());
		gc.setStroke(this.getColor());
		
		double segmentWidth = this.getLength() / this.numberSegments;
		double height = this.getLength() / 4;
		double xVal = 0;
		for (int i = 0; i < this.numberSegments; i++) {
			gc.fillOval(xVal, height / 4, segmentWidth, height / 2);
			xVal += segmentWidth - 2;
		}
		
		xVal = segmentWidth;
		for (int j = 0; j < this.getNumberLegs() / 2; j++) {
			gc.strokeLine(xVal, 0, xVal, height);
			xVal += segmentWidth - 2;
		}
	}
	
	/**
	 * Gets the drawing.
	 *
	 * @return the drawing
	 */
	public Canvas getDrawing() {
		double width = this.getLength();
		double height = width / 4;
		Canvas canvas = new Canvas(width, height);
		GraphicsContext gc = canvas.getGraphicsContext2D();	
		this.draw(gc);
		return canvas;
	}
	
	/**
	 * Gets the drawing.
	 *
	 * @param xCoord the xcoordinate of the upper left corner
	 * @param yCoord the ycoordinate of the upper left corner
	 * @return the drawing
	 */
	public Canvas getDrawing(double xCoord, double yCoord) {
		Canvas canvas = this.getDrawing();
		
		canvas.setLayoutX(xCoord);
		canvas.setLayoutY(yCoord);
		return canvas;
	}
	
	/**
	 * Gets the drawing height.
	 *
	 * @return the drawing height of the drawing
	 */
	public double getDrawingHeight() {
		return this.getDrawing().getHeight();
	}
	
	@Override
	public String getDescription() {
		String description = super.getDescription();
		description += System.lineSeparator();
		description += "Number segments: " + this.numberSegments;
		
		return description;
	}
	
	@Override
	public String toString() {
		return "Myriapoda length=" + this.getLength() + " #legs=" + this.getNumberLegs() + " #segments="
				+ this.numberSegments + " color=" + this.getColor();
	}
}
