package edu.westga.cs1302.bugcensus.viewmodel;

import java.io.File;
import java.util.List;

import edu.westga.cs1302.bugcensus.datatier.CensusDataReader;
import edu.westga.cs1302.bugcensus.model.Bee;
import edu.westga.cs1302.bugcensus.model.BeeCaste;
import edu.westga.cs1302.bugcensus.model.Bug;
import edu.westga.cs1302.bugcensus.model.BugCensus;
import edu.westga.cs1302.bugcensus.model.BugType;
import edu.westga.cs1302.bugcensus.model.Insect;
import edu.westga.cs1302.bugcensus.model.Myriapoda;
import edu.westga.cs1302.bugcensus.resources.UI;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.scene.canvas.Canvas;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

/**
 * The Class BugCensusViewModel.
 * 
 * @author CS1302
 * @version Fall 2018
 */
public class BugCensusViewModel {
	private BugCensus bugCensus;
	private Pane bugDisplay;
	private final ListProperty<Bug> bugsProperty;
	private StringProperty reportProperty;
	private ListProperty<BugType> bugTypeProperty;
	private ObjectProperty<BugType> selectedBugTypeProperty;
	private ListProperty<BeeCaste> beeCasteProperty;
	private ObjectProperty<BeeCaste> selectedBeeCasteProperty;
	private ObjectProperty<Color> bugColorProperty;
	private DoubleProperty lengthProperty;
	private IntegerProperty numberLegsProperty;
	private IntegerProperty numberSegmentsProperty;
	private BooleanProperty wingedProperty;
	
	/**
	 * Instantiates a new bug census view model.
	 * 
	 * @precondition bugDisplay != null
	 * @postcondition none
	 * 
	 * @param bugDisplay the pane displaying the bugs
	 */
	public BugCensusViewModel(Pane bugDisplay) {
		if (bugDisplay == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_BUGSPANE);
		}
		this.bugCensus = new BugCensus();
		this.bugDisplay = bugDisplay;
		this.bugsProperty = new SimpleListProperty<Bug>(FXCollections.observableArrayList(this.bugCensus.getBugs()));
		this.reportProperty = new SimpleStringProperty();
		this.bugTypeProperty = new SimpleListProperty<BugType>(FXCollections.observableArrayList(BugType.values()));
		this.selectedBugTypeProperty = new SimpleObjectProperty<BugType>();
		this.beeCasteProperty = new SimpleListProperty<BeeCaste>(FXCollections.observableArrayList(BeeCaste.values()));
		this.selectedBeeCasteProperty = new SimpleObjectProperty<BeeCaste>();
		this.bugColorProperty = new SimpleObjectProperty<Color>();
		this.lengthProperty = new SimpleDoubleProperty();
		this.numberLegsProperty = new SimpleIntegerProperty();
		this.numberSegmentsProperty = new SimpleIntegerProperty();
		this.wingedProperty = new SimpleBooleanProperty();
	}

	/**
	 * Gets the bugs property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the bugs property
	 */
	public ListProperty<Bug> bugsProperty() {
		return this.bugsProperty;
	}
	
	/**
	 * Returns report property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the report property
	 */
	public StringProperty reportProperty() {
		return this.reportProperty;
	}
	
	/**
	 * Gets the selected bug type property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the selected bug type property
	 */
	public ObjectProperty<BugType> selectedBugTypeProperty() {
		return this.selectedBugTypeProperty;
	}
	
	/**
	 * Gets the bug type property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the bug type property
	 */
	public ListProperty<BugType> bugTypeProperty() {
		return this.bugTypeProperty;
	}
	
	/**
	 * Gets the selected bee caste property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the selected bee caste property
	 */
	public ObjectProperty<BeeCaste> selectedBeeCasteProperty() {
		return this.selectedBeeCasteProperty;
	}
	
	/**
	 * Gets the selected bug color property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the selected bug color property
	 */
	public ObjectProperty<Color> bugColorProperty() {
		return this.bugColorProperty;
	}
	
	/**
	 * Gets the bee caste property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the bee caste property
	 */
	public ListProperty<BeeCaste> beeCasteProperty() {
		return this.beeCasteProperty;
	}

	/**
	 * Returns the length property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the length property
	 */
	public DoubleProperty lengthProperty() {
		return this.lengthProperty;
	}
	
	/**
	 * Returns the number legs property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the number legs property
	 */
	public IntegerProperty numberLegsProperty() {
		return this.numberLegsProperty;
	}
	
	/**
	 * Returns the number segments property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the number segments property
	 */
	public IntegerProperty numberSegmentsProperty() {
		return this.numberSegmentsProperty;
	}
	
	/**
	 * Returns the winged legs property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the winged property
	 */
	public BooleanProperty wingedProperty() {
		return this.wingedProperty;
	}
	
	/**
	 * Sets the pane for the display of bugs
	 * 
	 * @precondition bugDisplay != null
	 * @postcondition none
	 * 
	 * @param bugDisplay the pane displaying the bugs
	 */
	public void setBugDisplay(Pane bugDisplay) {
		if (bugDisplay == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_BUGSPANE);
		}
		this.bugDisplay = bugDisplay;
	}
	
	/**
	 * Displays the selected bug
	 * 
	 * @precondition selectedBug != null
	 * @postcondition none
	 * 
	 * @param selectedBug the bug to be displayed
	 */
	public void handleBugSelection(Bug selectedBug) {
		this.bugDisplay.getChildren().clear();
		if (selectedBug.getDrawingWidth() <= this.bugDisplay.getWidth()
				&& selectedBug.getDrawingHeight() <= this.bugDisplay.getHeight()) {
			Canvas drawing = selectedBug.getDrawing(0, 0);
			this.bugDisplay.getChildren().add(drawing);
		}
	}
	
	/**
	 * Displays the selected bug
	 * 
	 * @precondition selectedBug != null
	 * @postcondition none
	 * 
	 * @param selectedBugs the bugs to be displayed
	 */
	public void handleBugSelection(List<Bug> selectedBugs) {
		this.bugDisplay.getChildren().clear();
		for (Bug currBug: selectedBugs) {
			if (currBug.getDrawingWidth() <= this.bugDisplay.getWidth()
					&& currBug.getDrawingHeight() <= this.bugDisplay.getHeight()) {
				Canvas drawing = currBug.getDrawing(0, 0);
				this.bugDisplay.getChildren().add(drawing);
			}
		}
	}
	
	/**
	 * Adds a bug with specified attributes to the bug census.
	 *
	 * @return true if bug added successfully, false otherwise
	 */
	public boolean addBug() {
		Bug newBug = null;
		double length = this.lengthProperty.get();
		Color color = this.bugColorProperty.get();
		if (this.selectedBugTypeProperty.get() == BugType.INSECT) {
			Boolean winged = this.wingedProperty.get();
			newBug = new Insect(length, winged, color);
		} else if (this.selectedBugTypeProperty.get() == BugType.BEE) {
			BeeCaste caste = this.selectedBeeCasteProperty.get();
			newBug = new Bee(length, caste);
		} else if (this.selectedBugTypeProperty.get() == BugType.MYRIAPODA) {
			int numberLegs = this.numberLegsProperty.get();
			int numberSegments = this.numberSegmentsProperty.get();
			newBug = new Myriapoda(length, numberLegs, numberSegments, color);
		}
		
		if (this.bugCensus.add(newBug)) {
			this.resetAll();
		}
		return false;		
	}
	
	/**
	 * Generates and displays the summary report
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void generateSummaryReport() {
		String report = this.bugCensus.getSummaryReport();
		this.reportProperty.setValue(report);
	}
	
	/**
	 * Generates and displays the detailed report
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void generateDetailedReport() {
		String report = "";
		for (Bug bug : this.bugCensus.getBugs()) {
			report += "Type: " + bug.getClass().getSimpleName();
			report += System.lineSeparator();
			report += bug.getDescription();
			report += System.lineSeparator() + System.lineSeparator();
		}
		this.reportProperty.setValue(report);
	}
	
	/**
	 * Load census data from the specified file.
	 * 
	 * @precondition file != null
	 * @postcondition none
	 * 
	 * @param file
	 *            the file of census data to load
	 */
	public void loadCensusData(File file) {
		if (file == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_CENSUS_FILE);
		}
		CensusDataReader reader = new CensusDataReader(file);

		int year = reader.loadCensusYear();
		List<Bug> bugData = reader.loadCensusData();

		this.bugCensus = new BugCensus(year);
		for (Bug bug : bugData) {
			this.bugCensus.add(bug);
		}
		this.resetAll();
	}
	
	/**
	 * Resets bug form entries
	 */
	private void resetAll() {
		this.bugColorProperty.set(Color.BLACK);
		this.lengthProperty.set(0);
		this.numberLegsProperty.set(0);
		this.numberSegmentsProperty.set(0);
		this.wingedProperty.set(false);
		
		this.bugsProperty.set(FXCollections.observableArrayList(this.bugCensus.getBugs()));
		this.bugDisplay.getChildren().clear();
		this.reportProperty.set("");
	}
}
