package edu.westga.cs1302.bugcensus;
	
import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;


/**
 * The Class Main - entry point into the application.
 * 
 * @author CS1302
 * @version Fall 2018
 */
public class BugCensusApplication extends Application {
	private static final String WINDOW_TITLE = "Lab 5 by Darrell Branch";
	private static final String GUI_FXML = "view/BugCensusGui.fxml";

	/**
	 * Constructs a new Application object for the Bug Census demo program.
	 * 
	 * @precondition none
	 * @postcondition the object is ready to execute
	 */
	public BugCensusApplication() {
		super();
	}

	@Override
	public void start(Stage primaryStage) {
		try {
			Pane pane = this.loadGui();
			Scene scene = new Scene(pane);
			primaryStage.setScene(scene);
			primaryStage.setTitle(WINDOW_TITLE);
			primaryStage.setAlwaysOnTop(true);
			primaryStage.show();
		} catch (IllegalStateException | IOException anException) {
			anException.printStackTrace();
		}
	}

	/**
	 * Loads the GUI from the given FXML file.
	 * 
	 * @return the pane to be loaded
	 * @throws IOException
	 */
	private Pane loadGui() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource(GUI_FXML));
		return (Pane) loader.load();
	}

	/**
	 * Launches the application.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param args
	 *            not used
	 */
	public static void main(String[] args) {
		launch(args);
	}
}